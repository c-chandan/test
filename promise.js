// function http(url, method, succesCallback, errorHandler){
//   setTimeout (function(){
//     var data= "the alswer";
//     if(data){
//       succesCallback(data);
//     }
//     else{
//       errorHandler('No data');
//     }
//   },1000);
// }

// http('www.google.com', 'GET', function(data){
//   console.log(data);
// }, function(err){
//   console.log(err);
// });


// function http(url, method){
//     var promise = new Promise( function (resolve, reject){
//         setTimeout (function(){
//             var data= "hello ";
//             if(data){
//               resolve(data);
//             }
//             else{
//               reject('No data');
//             }
//           },1000);
//     });

//     return promise;
//   }
  
//   http('www.google.com', 'GET')
//   .then(function(data){
//       return data.toUpperCase();
//   })
//   .then(function(modificationData){
//       //console.log(modificationData);
//       return modificationData+ "hello chandan";
//   })
//   .then(function(concat){
//       console.log(concat);
//   })
//   .then(function(concat){
//     console.log(concat);
//     })
//   .catch(function(err){
//       console.log('Catch', err);
//   });


// let promise = new Promise(function(resolve, reject) {
//     setTimeout(() => resolve("done!"), 1000);
//   });

// let promise = new Promise(function(resolve, reject) {
//     // after 1 second signal that the job is finished with an error
//     setTimeout(() => reject(new Error("Whoops!")), 1000);
//   });

// let promise  = new Promise(function (resolve, reject){
//     //resolve("done");
//     reject(new Error ("..."))//
//     setTimeout(()=> resolve("..."),1000);
// })

// let promise = new Promise (function (resolve, reject){
//     //setTimeout(()=> resolve("done"),1000);
//     setTimeout(()=> reject(new Error("woops")),1000);
// });
// promise.then( result => console.log(result),
//  error => console.log(error)
// );

// an "immediately" resolved Promise


// new Promise(function(resolve, reject) {

//     setTimeout(() => resolve(1), 1000); // (*)
  
//   }).then(function(result) { // (**)
  
//     console.log(result); // 1
//     return result * 2;
  
//   }).then(function(result) { // (***)
  
//     console.log(result); // 2
//     return result * 2;
  
//   }).then(function(result) {
  
//     console.log(result); // 4
//     return result+10;
  
//   }).then(function(result){
//       console.log(result);
//   });



// new Promise (function (resolve, reject){
//     setTimeout(()=> resolve("done"),1000);
// }).then(function(result){
//     console.log(result);
//     new Promise(function(resolve, reject){
//         resolve(result+" what..!");
        
//     })
// }).then(function(result){
//     console.log(result);
// })

// let promise = new Promise (function (resolve, reject){
//     setTimeout(()=> resolve(1),1000);
// });

// promise.then(function(result){
//     console.log(result);
//     return result*2;
// }).then(function(result){
//     console.log(result);
//     return result*2;
// }).then(function(result){
//     console.log(result);
//     return result*20;
// }).then(function(result){
//     console.log(result);

// })



// new Promise(function (resolve, reject){
//     setTimeout(()=> resolve(1), 1000);
// }).then(function(result){
//     console.log(result);
//     return new Promise((resolve, reject)=>{
//         setTimeout(() => resolve(result*2),1000);
//     });
// }).then(function(result){
//     console.log(result);
//    return new Promise((resolve, reject)=>{
//        setTimeout(()=> resolve(result*10));
//    })
    
// }).then(function(result){
//     console.log(result);
// })


// var fs = require('fs');
// function fetch(url){
//     var promise = new Promise(function (resolve ,reject){
//         setTimeout(()=>{
//            var result= fs.readFileSync(url);
//            //console.log(result.toString());
//             resolve(result.toString());
//         },1000)

//     })
//  return promise;
// }

// fetch('/media/chandan/other/C-Task/data.json')
//   //.then below runs when the remote server responds
//   .then(function(response) {
//     // response.text() returns a new promise that resolves with the full response text
//     // when we finish downloading it
//    console.log(response);
//     return response;
//   })
  
const http = require('http');
const request = require('request');
const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World\n');
  });


var fs = require('fs');
function fetch(url){
    var promise = new Promise(function (resolve ,reject){
        setTimeout(()=>{
           var result= fs.readFileSync(url);
            resolve(result.toString());
        },1000)

    })
 return promise;
}

fetch('/media/chandan/other/C-Task/data.json')
  .then(function(response){
    var data = JSON.parse(response);
    return data.name;

  }).then(function(userName){
    request(`https://api.github.com/users/${userName}`, function(err, response){
        if(err){
            console.log('something went wrong');
        }
        console.log(response.toJSON());
    });
  })




  server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
  });